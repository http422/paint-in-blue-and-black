
#include <stdio.h>

#include <GL\glut.h>

//-------------------------------------------------------------------
class Point2 {
  public:
    Point2()                         { x =      y = 0.0f;}
    Point2(float xIn, float yIn)     { x = xIn; y = yIn; }

    void  set( float xIn, float yIn) { x = xIn; y = yIn; }
    float getX(void) { return x;}
    float getY(void) { return y;}
    void  draw(void) { glBegin(GL_POINTS);
                         glVertex2f( (GLfloat) x, (GLfloat) y);
                       glEnd();
                     }
  private:
    float x;
    float y;
};
//-------------------------------------------------------------------
class IntRect {
  public:
    IntRect() { left = 0; right = 0; bottom = 0; top = 100; }
    IntRect( int leftIn, int rightIn, int bottomIn, int topIn)
              { left = leftIn; right = rightIn; bottom = bottomIn; top = topIn; }

    void set(int leftIn, int rightIn, int bottomIn, int topIn)
              { left = leftIn; right = rightIn; bottom = bottomIn; top = topIn; }
    float aspectRatio(void) { return (float) (right-left) / (float) (top-bottom); }
    void draw(void) { glRecti( left, bottom, right, top ); }

  private:
    int left;
    int right;
    int bottom;
    int top;
};
//-------------------------------------------------------------------
class RealRect {
  public:
    RealRect() { left = 0; right = 0; bottom = 0; top = 100; }
    RealRect( float leftIn, float rightIn, float bottomIn, float topIn)
              { left = leftIn; right = rightIn; bottom = bottomIn; top = topIn; }

    void set(float leftIn, float rightIn, float bottomIn, float topIn)
              { left = leftIn; right = rightIn; bottom = bottomIn; top = topIn; }
    float aspectRatio(void) { return (right-left) / (top-bottom); }

    void draw(void) { glRectf( left, bottom, right, top ); }

  private:
    float left;
    float right;
    float bottom;
    float top;
};
//-------------------------------------------------------------------
class Canvas {
  public:
    Canvas(int windowWidth, int windowHeight, char * windowTitle);

    void setWindow( float left, float right, float bottom, float top);
    void setViewport( int left, int right, int bottom, int top);
    IntRect  getViewport(void);
    RealRect getWindow(void);
    float    getWindowAspectRatio(void);

    void clearScreen(void);
    void setBackgroundColor(float red, float green, float blue);
    void setColor          (float red, float green, float blue);
    
    void lineTo(float x, float y);
    void lineTo(Point2 p);

    void moveTo(float x, float y);
    void moveTo(Point2 p);

	void moveRel(float dx, float dy);
	void lineRel(float dx, float dy);

	void turnTo(float angle);
	void turn(float angle);
	void forward(float dist, int isVisible);

	void setCurrentDirection(float CDin){currentDirection=CDin;}
	float getCurrentDirection(void){return currentDirection;}

	Point2 getCurrentPosition(void); 

	void drawArc(Point2 center, float radius, float startAngle,
				float sweep);
	void drawArc(Point2 center, float radius, float startAngle,
					 float sweep, float filled_polyline);

	void YingYang(Point2 center, float radius,
				  float startAngle);


  private:
    Point2    currentPosition;
    IntRect   viewport;
    RealRect  window;
	GLfloat currentDirection;
};

