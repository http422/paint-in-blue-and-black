#include <GL\glut.h>
#include "ACanvas.h"

const int windowHeight=480;
const int windowWidth=640;

int m=0;		//counter for incrementing array A
int n=0;		//counter for incrementing array B
Point2 A[20];	//declaration for array A
Point2 B[20];	//declaration for array B

Canvas myCanvas(windowWidth, windowHeight, "Tweety Tween");


//-------------------------------------------------------------------

void setMN(int  mIn, int nIn)
				//this function resets/sets the array index
{
	m=mIn;
	n=nIn;
}

//-------------------------------------------------------------------

void setArrayA(int x, int y)
				//sets the array A values
{
	A[m].set(x,y);
	m++;
}

//-------------------------------------------------------------------

void setArrayB(int x, int y)
				//sets the array B values
{
	B[n].set(x,y);
	n++;
}

//-------------------------------------------------------------------

void myMouse(int button, int state, int x, int y)
				//upon left clicks screen displays a polyline
				//and stores the coordinates into array A
				//upon right clicks screen displays blue 
				//polyline and stores coordinates into array B
				//m and n are used as array indexes
{
	if(button==GLUT_LEFT_BUTTON && state==GLUT_DOWN)
	{
		glColor3f(0.0, 0.0, 0.0);
		if(m==0)
			myCanvas.moveTo(x,windowHeight-y);
		else
			myCanvas.lineTo(x,windowHeight-y);
		setArrayA(x, windowHeight-y);
		glutSwapBuffers();
	}


	else if(button==GLUT_RIGHT_BUTTON && state==GLUT_DOWN)
	{
		glColor3f(0.0, 0.0, 1.0);
		if(n==0)
			myCanvas.moveTo(x,windowHeight-y);
		else
			myCanvas.lineTo(x,windowHeight-y);
		setArrayB(x, windowHeight-y);
		glutSwapBuffers();
	}
		
}

//-------------------------------------------------------------------

void myKeyboard(unsigned char key, int x, int y)
				//function displays array A’s polyline when
				//user presses ‘1’, B’s polyline when user //presses ‘2’, both at ‘r’, tweens the 2
				//polylines at ‘t’, clears the screen at ‘c’ 
{
	switch(key)
	{
	case 't':
		if(m!=n)
		{
			if(n<m)
				myCanvas.drawAllFrames(A, B, n); 	
			else
				myCanvas.drawAllFrames(A, B, m); 	
		}

		else
				myCanvas.drawAllFrames(A, B, m);
		break;
	case 'c':
		myCanvas.clearScreen();
		setMN(0, 0);
		break;

	case '1':
		myCanvas.clearScreen();		
		myCanvas.drawA(A, m);
		break;

	case '2':
		myCanvas.clearScreen();
		myCanvas.drawB(B, n);
		break;

	case 'r':
		myCanvas.clearScreen();	
		myCanvas.drawA(A, m);
		myCanvas.drawB(B, n);
		break;
		
	default:
		break;
	}
}

//-------------------------------------------------------------------

void myDisplay(void)
{
  myCanvas.clearScreen();
  glFlush();
  glutSwapBuffers(); 
}

//-------------------------------------------------------------------

void main(void)
{
  myCanvas.setBackgroundColor(1.0, 1.0, 1.0); // white
  myCanvas.setColor(0.0, 0.0, 0.0); // black

  myCanvas.setWindow(0.0, 640.0, 0.0, 480.0);
  myCanvas.setViewport(0, 480, 0, 480);

  glutDisplayFunc(myDisplay);
  glutKeyboardFunc(myKeyboard);
  glutMouseFunc(myMouse);

  glutMainLoop();
}


